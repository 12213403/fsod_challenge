import os
import sys
import logging

import numpy as np
import torch
from torchvision.ops import batched_nms

from pycocosiou.coco import COCO
from pycocosiou.cocoeval import COCOeval

from ..utils import tqdm, HiddenPrints



class Evaluator():
    """ 
    Evaluation class. 
    Performs evalution on a given model and DataHandler. 

    Two main functions can be used: 
        - eval: which runs evaluation on the DataHandler query set.
        - eval_all: which use DataHandler instantiated with eval_all=True. 
                    It runs eval on all the loaders provided by the DataHandler.  
    
    Arguments:
        - model: pytorch model to be evaluated.
        - cfg: cfg object of the model.
        - data_handler: DataHandler object of the dataset.
    """
    def __init__(self,
                 model,
                 cfg,
                 data_handler,
                 output_folder='./Experiments_FSFCOS/Results',
                 criterions=['iou'],
                 pb=None):
        self.model = model
        self.cfg = cfg

        self.data_handler = data_handler
        self.device = cfg.MODEL.DEVICE
        self.output_folder = output_folder

        self.categories =  None
        self.current_classes = None
        self.criterions = criterions
        self.logger = logging.getLogger()

        self.pb = pb
        self.episode_task = None
        self.mini_episode_task = None


    def eval(self, verbose=True, all_classes=True, verbose_classes=True, loaders=None, seed=None):
        """
        Eval function on a single data loader (or couple query/support loaders)

        Arguments:
            - verbose: print eval results at the end of computation.
            - all_classes:  
        """
        if seed is not None:
            self.data_handler.rng_handler_free.update_seeds(seed)

        if self.cfg.FEWSHOT.ENABLED and not self.cfg.FEWSHOT.NO_FS_PRETRAIN:
            if loaders is None:
                query_loader, support_loader, classes = self.data_handler.get_dataloader(
                    seed=seed
                )
            else:
                query_loader, support_loader, classes = loaders

            self.current_classes = classes
            if verbose_classes:
                self.logger.info('Evaluation on classes: {}'.format(str(classes)))

            self.categories = {
                idx: v['name']
                for idx, v in query_loader.dataset.coco.cats.items()
            }

            self.contiguous_label_map = query_loader.dataset.contiguous_category_id_to_json_id


            predictions = self.compute_pred_fs(query_loader, support_loader,
                                            classes)
        else:
            query_loader = self.data_handler.get_dataloader(seed=seed)
            self.contiguous_label_map = query_loader.dataset.contiguous_category_id_to_json_id
            classes = np.array(list(query_loader.dataset.coco.cats.keys())) + 1
            predictions = self.compute_pred(query_loader)



        if self.has_pred(predictions):

            for pred in predictions:
                pred.add_field("objectness",
                                torch.ones(len(pred), device=self.device))

            # dirty hack to remove prints from pycocotools
            with HiddenPrints(True):

                coco_results = self.prepare_for_coco_detection(predictions,
                                                    query_loader.dataset)

                res = self.evaluate_predictions_on_coco(
                    query_loader.dataset.coco, coco_results,
                    os.path.join(self.output_folder, 'res.json'),
                    'bbox',
                    classes=classes)

                res_per_class = {}
                for c in classes:
                    res_per_class[c] = self.evaluate_predictions_on_coco(query_loader.dataset.coco,
                                                            coco_results,
                                                            os.path.join(
                                                                self.output_folder,
                                                                'res.json'),
                                                            'bbox',
                                                            classes=[c])
            if verbose:
                self.eval_summary(res, res_per_class, all_classes=all_classes)
            return res, res_per_class

        else:
            return {'iou': {}}, {'iou': {}}

    def eval_all(self, n_episode=1, verbose=True, seed=None):
        """
        Similar to eval function except it loop over the multiple dataloaders returned 
        by the DataHandler. (DataHandler must have eval_all=True).

        Results are then accumulated and stored in a pandas dataframe. 
        
        """
        assert self.data_handler.eval_all == True, 'Use eval_all with eval_all=True in DataHandler'

        all_res = {c :{
                        'train': {},
                        'test': {}
                    } for c in self.criterions}
        if seed is not None:
            self.data_handler.rng_handler_free.update_seeds(seed)


        self.pb.reset(self.pb.episode_task)

        for eval_ep in range(n_episode):
            loaders = self.data_handler.get_dataloader(seed=seed)
            for setup in ['train', 'test']:
                res_all_cls = {}
                for q_s_loaders in loaders[setup]:
                    # Important to set seed to None here so that same exemples are not sampled
                    # at each episode
                    _, res_cls = self.eval(verbose=False, verbose_classes=verbose, loaders=q_s_loaders, seed=None)
                    # this will overwrite some keys if the last batch is padded
                    # but only one eval is retained for each class
                    res_all_cls.update(res_cls)

                for k, v in res_all_cls.items():
                    for criterion, res_all_per_criterion in v.items():
                        if not k in all_res[criterion][setup]:
                            all_res[criterion][setup][k] = []
                        all_res[criterion][setup][k].append(
                            res_all_per_criterion.stats)
            self.pb.advance(self.pb.episode_task)

        for setup in ['train', 'test']:
            for criterion in self.criterions:
                for k, v in all_res[criterion][setup].items():
                    all_res[criterion][setup][k] = np.vstack(
                        all_res[criterion][setup][k])
                    all_res[criterion][setup][k] = np.where(
                        all_res[criterion][setup][k] == -1, np.nan,
                        all_res[criterion][setup][k]).mean(axis=0)


        return {c:
            self.prettify_results_fs(all_res_per_criterion, verbose=verbose)
            for c, all_res_per_criterion in all_res.items()
        }


    def eval_no_fs(self, seed=None, verbose=False):
        """
        Eval without fewshot.  
        """
        overall_res, res_per_class = self.eval(seed=seed, verbose=verbose)
        for k in res_per_class:
            res_per_class[k] = res_per_class[k].stats

        return self.prettify_results(res_per_class, verbose=verbose)

    def eval_summary(self, res, res_per_class, all_classes=True):
        """
        Format results.  
        """
        for i in range(len(res)):
            classes = list(res_per_class[i].keys())
            sep = '+{}+'.format('-'*77)
            print('''\n{}\n\t\tAll classes {:<30}\n{}'''.format(
                        sep,
                        self.get_cat_names(classes),
                        sep))
            res[i].summarize()
            if all_classes:
                for c, res in res_per_class[i].items():
                    print('''\n{}\n\t\tClass {}\n{}'''.format(
                        sep,
                        '{}: '.format(c) + self.categories[c-1],
                        sep))
                    res[i].summarize()

    def get_cat_names(self, classes):
        return ", ".join([
            '\n\t\t{}: '.format(c) + self.categories[c-1] for c in classes
        ])


    def compute_pred_fs(self, query_loader, support_loader=None, classes=None):
        """
        Model inference on a query_loader with fewshot. 
        """
        predictions = []
        with torch.no_grad():

            support = self.model.compute_support_features(
                    support_loader, self.device)

            self.pb.reset(self.pb.mini_episode_task,
                              description='[red]Episode {}'.format(classes),
                              total=len(query_loader))
            for iteration, (images, targ,
                            img_id) in enumerate(query_loader):

                if self.cfg.TEST.BBOX_AUG.ENABLED:
                    pred_batch = []
                    orig_sizes = images.image_sizes
                    for s in self.cfg.TEST.BBOX_AUG.SCALES:
                        images.tensors = torch.nn.functional.interpolate(images.tensors, (s,s))
                        images.image_sizes = [torch.Size((s, s))
                                              for _ in images.image_sizes]
                        images = images.to(self.device)
                        pred_scale = self.model(images,
                                                classes=classes,
                                                support=support)
                        if len(pred_batch) == 0:
                            pred_resized = []
                            for idx, pred in enumerate(pred_scale):
                                pred_resized.append(pred.resize(orig_sizes[idx]))
                            pred_batch = pred_resized
                        else:
                            for idx, pred in enumerate(pred_scale):
                                pred = pred.resize(orig_sizes[idx])
                                pred_batch[idx].bbox = torch.cat([pred_batch[idx].bbox, pred.bbox])
                                for k, v in pred_batch[idx].extra_fields.items():
                                    pred_batch[idx].extra_fields[k] = torch.cat([v, pred.extra_fields[k]])
                    for idx, pred in enumerate(pred_batch):
                        keep_ind = batched_nms(pred.bbox,
                                                      pred.get_field('scores'),
                                                      pred.get_field('labels'),
                                                      self.cfg.MODEL.FCOS.NMS_TH)
                        pred_batch[idx].bbox = pred_batch[idx].bbox[keep_ind]
                        for k, v in pred_batch[idx].extra_fields.items():
                            pred_batch[idx].extra_fields[k] = v[keep_ind]
                else:
                    images = images.to(self.device)
                    pred_batch = self.model(images, classes=classes, support=support)
                for idx, pred in enumerate(pred_batch):
                    pred.add_field('image_id', torch.tensor(
                        img_id[idx]))  # store img_id as tensor for convenience
                    predictions.append(pred.to('cpu'))
                self.pb.advance(self.pb.mini_episode_task)

        return predictions

    def compute_pred(self, query_loader, support_loader=None, classes=None):
        """
        Model inference on a query_loader without fewshot. 
        """
        predictions = []
        with torch.no_grad():
            for iteration, (images, targ, img_id) in enumerate(query_loader):
                images = images.to(self.device)
                pred_batch = self.model(images)
                for idx, pred in enumerate(pred_batch):
                    pred.add_field('image_id', torch.tensor(
                        img_id[idx]))  # store img_id as tensor for convenience
                    predictions.append(pred.to('cpu'))

        return predictions

    def evaluate_predictions_on_coco(self,
        coco_gt, coco_results, json_result_file, iou_type="bbox",
        classes=None):
        """
        Run coco evaluation using pycocotools. 
        """
        coco_dt = coco_gt.loadRes(coco_results)
        # self.ignore_dataset_annot_without_pred(coco_gt, coco_dt, classes)

        coco_evals = {c: COCOeval(coco_gt,
                                coco_dt,
                                iou_type,
                                criterion=c) for c in self.criterions}

        for c, coco_eval in coco_evals.items():
            coco_eval.params.catIds = [self.contiguous_label_map[c] for c in classes]
            coco_eval.params.imgIds = list(set([
                det['image_id'] for det in list(coco_dt.anns.values())
            ]))
            coco_eval.evaluate()
            coco_eval.accumulate()
            coco_eval.summarize()

        return coco_evals

    def ignore_dataset_annot_without_pred(self, coco_gt, coco_dt, classes):
        img_with_predictions = set([det['image_id'] for det in list(coco_dt.anns.values())])
        gt_anns = coco_gt.anns
        classes_json = [self.contiguous_label_map[c] for c in classes]
        rm_keys = []
        for k, v in gt_anns.items():
            if v['image_id'] not in img_with_predictions or \
                (classes is not None and v['category_id'] not in classes_json):
                # category id is not necesarily contiguous
                gt_anns[k]['ignore'] = 1
                rm_keys.append(k)
            elif v['image_id'] not in img_with_predictions:
                del coco_gt.imgs[v['image_id']]

        for k in rm_keys:
            del gt_anns[k]

    def prepare_for_coco_detection(self, predictions, dataset):
        """
        Convert predictions from model into coco format detections. 
        """
        # assert isinstance(dataset, COCODataset)
        coco_results = []
        for prediction in predictions:
            image_id = prediction.get_field('image_id').item()
            original_id = dataset.id_to_img_map[image_id]
            if len(prediction) == 0:
                continue

            img_info = dataset.get_img_info(image_id)
            image_width = img_info["width"]
            image_height = img_info["height"]
            prediction = prediction.resize((image_width, image_height))
            prediction = prediction.convert("xywh")

            boxes = prediction.bbox.tolist()
            scores = prediction.get_field("scores").tolist()
            labels = prediction.get_field("labels").tolist()

            mapped_labels = [dataset.contiguous_category_id_to_json_id[i] for i in labels]
            coco_results.extend(
                [
                    {
                        "image_id": original_id,
                        "category_id": mapped_labels[k],
                        "bbox": box,
                        "score": scores[k],
                    }
                    for k, box in enumerate(boxes)
                ]
            )
        return coco_results

    def has_pred(self, predictions):
        nb_pred = 0
        for pred in predictions:
            nb_pred += len(pred)

        return nb_pred > 0


    """
    Prettify method build pandas dataframes from results of evaluation. 
    """
    def prettify_results_fs(self, results, verbose=True):
        import pandas as pd
        metrics = {}

        metrics['Measure'] = ['AP'] * 6 + ['AR'] * 6
        metrics['IoU'] = [
            '0.50:0.95',
            '0.50',
            '0.75',
        ] + ['0.50:0.95'] * 9
        metrics['Area'] = ['all', 'all', 'all', 'small', 'medium', 'large'] * 2

        df_metrics = pd.DataFrame.from_dict(metrics)

        df_train = pd.DataFrame.from_dict(results['train'])
        df_train = df_train.reindex(sorted(df_train.columns), axis=1)
        df_test = pd.DataFrame.from_dict(results['test'])
        df_test = df_test.reindex(sorted(df_test.columns), axis=1)

        train_cls = list(results['train'].keys())
        df_all = pd.concat([df_metrics, df_train, df_test], axis=1)

        df_all = df_all.set_index(['Measure', 'IoU', 'Area'])
        columns = [('Train classes', c) if c in train_cls else
                   ('Test classes', c) for c in df_all.columns]
        df_all.columns = pd.MultiIndex.from_tuples(columns)

        if verbose:
            print(df_all)
        return df_all

    def prettify_results(self, results, verbose=True):
        import pandas as pd
        metrics = {}

        metrics['Measure'] = ['AP'] * 6 + ['AR'] * 6
        metrics['IoU'] = [
            '0.50:0.95',
            '0.50',
            '0.75',
        ] + ['0.50:0.95'] * 9
        metrics['Area'] = ['all', 'all', 'all', 'small', 'medium', 'large'] * 2

        df_metrics = pd.DataFrame.from_dict(metrics)

        df_train = pd.DataFrame.from_dict(results)
        df_train = df_train.reindex(sorted(df_train.columns), axis=1)


        train_cls = list(results.keys())
        df_all = pd.concat([df_metrics, df_train], axis=1)

        df_all = df_all.set_index(['Measure', 'IoU', 'Area'])
        columns = [('Train classes', c) for c in df_all.columns]
        df_all.columns = pd.MultiIndex.from_tuples(columns)

        if verbose:
            print(df_all)
        return df_all