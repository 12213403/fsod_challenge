import os
import sys
import time
import logging
import numpy as np
from datetime import datetime
from torch.utils.tensorboard import SummaryWriter
from rich.progress import Progress, TextColumn, BarColumn,MofNCompleteColumn, TimeRemainingColumn, TimeElapsedColumn, SpinnerColumn

'''
Wrapper for the tensorboard summary writer which creates a directory for 
each study/run so that it is easier to display in tensorboard
'''


class CustomLogger():
    def __init__(self, log_dir='./runs/', new_dir=True, notify=False):
        if sys.gettrace() is None:
            if not os.path.isdir(log_dir):
                os.makedirs(log_dir)

            self.log_dir = log_dir

            self.new_dir = new_dir

            if new_dir:
                name = str(datetime.fromtimestamp(time.time())).replace(
                    ' ', '_')
                self.log_dir = os.path.join(log_dir, 'study_' + name)
                self.dir = os.path.join(self.log_dir, 'runs_' + name)
                os.makedirs(self.dir)
            else:
                name = str(datetime.fromtimestamp(time.time())).replace(
                    ' ', '_')
                self.dir = os.path.join(log_dir, 'runs_' + name)

            self.writer = SummaryWriter(log_dir=self.dir)

    def new_run(self):
        assert self.new_dir, 'Logger is not setup to handle runs reseting, use new_dir=True instead.'
        name = str(datetime.fromtimestamp(time.time())).replace(' ', '_')
        self.dir = os.path.join(self.log_dir, 'runs_' + name)
        self.writer = SummaryWriter(log_dir=self.dir)

    def add_multi_scalars(self, dict_scalars, iteration, main_tag='Losses'):
        for k, v in dict_scalars.items():
            if hasattr(self, 'writer'):
                self.writer.add_scalar('{}/{}'.format(main_tag, k), v, iteration)



class ProgressBar(Progress):
    def __init__(self):
        columns_progress = [
            TextColumn("[progress.description]{task.description}"),
            BarColumn(),
            MofNCompleteColumn(),
            TimeRemainingColumn(),
            TimeElapsedColumn(),
            SpinnerColumn(),
        ]
        super().__init__(*columns_progress)
