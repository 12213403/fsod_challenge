# Challenge Dataset

The dataset provided for this challenge is a modified version of DOTA dataset.
Specifically, it has been converted into the COCO format with a single
annotation file for each dataset split. The images have been tiled into 512x512
patches for easier processing. And finally, the splits have been randomly resampled. 

## Download

The dataset is available at this [url]().

## File structure 

Images of each split are stored in separated folders (`train`, `val`, and
`test`). The annotations for all images of a split are provided inside a `json`
annotation file following the coco format (detailed
[here](https://github.com/cocodataset/cocoapi)). The annotations files are
stored under the `annotation` folder.

```
.
└── dataset_root
    ├── annotations
    │   ├── instances_train.json
    │   ├── instances_val.json
    │   └── instances_test.json
    ├── train
    ├── val
    ├── test
    └── class_split.txt
```

In addition, a `class_split.txt` file at the dataset root allows to specify what
class is considered a base class or a novel class. The file contains two lines,
one listing the ids of the base classes and one listing the ids of the novel
classes, comma speparated.




