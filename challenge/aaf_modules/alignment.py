from detlaf.aaf_framework.utils import AAFIOMode
from detlaf.utils import registry
from detlaf.aaf_framework.alignment import BaseAlignment

import torch
from torch import nn
import torch.nn.functional as F

@registry.ALIGNMENT_MODULE.register("CISA_CHALLENGE")
class CISAChallenge(BaseAlignment):
    """
    CISA block for Dual-Awareness Attention for
    Few-Shot Object Detection
    (https://arxiv.org/abs/2102.12152)
    """
    def __init__(self, *args, in_ch=256):
        super(CISAChallenge, self).__init__(*args)

        self.W_r = nn.Linear(in_ch, 1)
        self.W_k = nn.Linear(in_ch, in_ch // 4)
        self.W_q = nn.Linear(in_ch, in_ch // 4)

        self.in_mode = AAFIOMode.Q_BCHW_S_NCHW
        self.out_mode = AAFIOMode.Q_BNCHW_S_NBCHW

        self.query_attended_features = None

    def forward(self, features):
        query_features = features['query' + self.input_name]
        support_features = features['support' + self.input_name]
        support_targets = features['support_targets']

        support_aligned_ = []
        query_aligned_ = []
        self.attention_map = []

        K = self.cfg.FEWSHOT.K_SHOT

        for query, support in zip(query_features, support_features):

            B, C, Hq, Wq = query.shape
            # N_support should be N_WAYS*K_SHOT * B
            # but if SAME_SUPPORT_IN_BATCH is False, it could be
            # N_WAYS*K_SHOT only.
            N_support, C, Hs, Ws = support.shape

            # if N_support % (B * self.cfg.FEWSHOT.K_SHOT) != 0 or N_support // (
            #         B * self.cfg.FEWSHOT.K_SHOT) == 1:
            if (N_support // K == B) or (N_support % (B * self.cfg.FEWSHOT.K_SHOT) != 0):
                # support = support.repeat(B, 1, 1, 1)
                support = support.repeat_interleave(B, dim=0)
                N_support, C, Hs, Ws = support.shape

            query = query.repeat(N_support // B, 1, 1, 1) #N, C, Hq, Wq

            query = query.flatten(start_dim=-2).permute(0, 2, 1)  # N, HqWq, C
            support = support.flatten(start_dim=-2).permute(0, 2,1)  # N, HsWs, C
            # print(query.shape, support.shape)

            query_mapped = self.W_q(query) # N, HqWq, C'
            support_mapped = self.W_k(support).permute(0, 2, 1)  # N, C', HsWs
            query_mapped = query_mapped - query_mapped.mean()
            support_mapped = support_mapped - support_mapped.mean()

            query_support_sim = torch.bmm(query_mapped, support_mapped) # N, HqWq, HsWs
            query_support_sim = query_support_sim / Hq
            query_support_sim = F.softmax(
                query_support_sim.view(B, -1),
                dim=-1).view_as(query_support_sim)  # N, HqWq, HsWs

            support_self = F.softmax(self.W_r(support), dim=1) # N, HsWs, 1
            query_support_sim = query_support_sim + 0.1 * support_self.permute(
                0, 2, 1)  # B, HqWq, HsWs

            support_aligned = torch.bmm(query_support_sim, support) # N, HqWq, C
            support_aligned = support_aligned.view(
                N_support//B//K, K, B, Hq, Wq, C).mean(dim=1).permute(0,1,4,2,3) # N_WAYS*K_SHOT, B, C, Hq, Wq

            query_aligned = query.view(
                N_support // B // K, K, B, Hq, Wq, C).mean(dim=1).permute(1, 0, 4, 2, 3)

            support_aligned_.append(support_aligned)
            query_aligned_.append(query_aligned)

        self.query_attended_features = support_aligned_
        features.update({
            'query' + self.output_name: query_aligned_,
            'support' + self.output_name: support_aligned_
        })